#include <stdio.h>
#include <stdlib.h>

void swapnum (int *var1, int *var2)
{
    int tempnum ;
    tempnum = *var1 ;
    *var1 = *var2 ;
    *var2 = tempnum ;
}
int main()
{
    int num1= 27, num2=53;
    printf("Before swapping :");
    printf("\n num1 value is %d :",num1);
    printf("\n num2 value is %d:",num2);

    swapnum(&num1, &num2);

    printf("\nAfter swapping:");
    printf("\n num1 value is %d",num1);
    printf("\n num2 value is %d",num2);
    return 0;
}