#include <stdio.h>
#include <stdlib.h>

void add(int *a,int *b,int *s);
void sub(int *a,int *b,int *dif);
void mult(int *a,int *b,int *pro);
void divi(int *a,int *b,int *d);
void rem(int *a,int *b,int *r);
int main()
{
    int num1,num2,sum,dif,pro,d,r;
    printf("Enter 2 numbers :\n");
    scanf("%d %d",&num1,&num2);
    add(&num1,&num2,&sum);
    printf("Sum of 2 numbers is = %d\n",sum);
    sub(&num1,&num2,&dif);
    printf("Difference of 2 numbers is = %d\n",dif);
    mult(&num1,&num2,&pro);
    printf("Product of 2 numbers is = %d\n",pro);
    divi(&num1,&num2,&d);
    printf("Division of 2 numbers is = %d\n",d);
    rem(&num1,&num2,&r);
    printf("Remainder of 2 numbers is = %d\n",r);
    return 0;
}
void add(int *a,int *b,int *s)
{
    *s= *a+*b;
}
void sub(int *a,int *b,int *dif)
{
    *dif= *a-*b;
}
void mult(int *a,int *b,int *pro)
{
    *pro= (*a)*(*b);
}
void divi(int *a,int *b,int *d)
{
    *d= (*a)/(*b);
}
void rem(int *a,int *b,int *r)
{
    *r= (*a)%(*b);
}