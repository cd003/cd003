#include <stdio.h>
#include<math.h>

int main()
{
    int i,n;
    float term,sum=0;
    printf("enter the value of n:");
    scanf("%d",&n);

    for(i=1;i<=n;i++)
    {
        term=pow(i,2);
        sum=sum+term;
    }
    printf("sum of series is 1^2 + 2^2+....=%0.2f",sum);

    return 0;
}
